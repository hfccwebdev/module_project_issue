<?php

/**
 * Page callback for the issue statistics page.
 */
function project_issue_statistics($project = NULL) {
  $states = project_issue_state();
  $output = '';
  if (!empty($project->nid)) {
    $filter = sprintf('AND p.pid = %d', (int)$project->nid);
    project_project_set_breadcrumb($project, TRUE);
    $output .= project_issue_query_result_links($project->project['uri']);
  }
  else {
    $output .= project_issue_query_result_links();
    $filter = '';
  }

  $month = time() - 30 * 86400;
  $year = time() - 365 * 86400;

  $category_duration = array();
  $category_count = array();
  $all_duration = array();
  $all_count = array();
  $all_stages = array();
  $all_stages_count = array();
  $duration_count = array(
    'two_days' => array('label' => t('less than two days')),
    'two_weeks' => array('label' => t('two days to two weeks')),
    'two_months' => array('label' => t('two weeks to two months')),
    'six_months' => array('label' => t('two to six months')),
    'over_six_months' => array('label' => t('more than six months')),
  );
  $duration_total = array();

  $output .= '<div class="project-issue">';

  $result = db_query('SELECT n.nid, n.title, p.category, n.created, p.sid FROM {project_issues} p INNER JOIN {node} n ON n.nid = p.nid WHERE n.status = 1 %s ORDER BY n.nid', $filter);
  $issues = array();
  while ($issue = db_fetch_object($result)) {
    $comments = db_query('SELECT cid, sid, timestamp FROM {project_issue_comments} WHERE nid = %d ORDER BY nid, cid', $issue->nid);
    $duration = 0;
    $stages = array();
    $last_stage = 1;
    $last_date = $issue->created;
    $status = 1;
    $is_closed = FALSE;
    while ($comment = db_fetch_object($comments)) {
      //  4 - postponed
      // 16 - needs upstream fix
      // 15 - needs more info
      //  1 - active
      //  8 - needs review
      // 14 - ready for live
      //  2 - fixed
      //  3 - duplicate
      //  5 - won't fix
      //  6 - by design
      //  7 - closed

      // First attempt, figure out *actual* duration!
      // @todo: break down by stages (waiting on who?)

      if ($comment->sid == 2) {
        if ($status !== 2) {
          $status = 2;
          $duration = $comment->timestamp - $issue->created;
        }
        $is_closed = TRUE;
      }
      elseif ($comment->sid == 3) {
        $duration = 0;
        $status = 3;
        $is_closed = TRUE;
      }
      elseif ($comment->sid == 4) {
        // for now...
        $duration = 0;
        $status = 4;
        $is_closed = TRUE;
      }
      elseif ($comment->sid == 5) {
        if ($status !== 5) {
          $status = 5;
          $duration = $comment->timestamp - $issue->created;
        }
        $is_closed = TRUE;
      }
      elseif ($comment->sid == 6) {
        if ($status !== 6) {
          $status = 6;
          $duration = $comment->timestamp - $issue->created;
        }
        $is_closed = TRUE;
      }
      elseif ($comment->sid == 7) {
        if ($status !== 7 && $status !== 2) {
          $status = 7;
          $duration = $comment->timestamp - $issue->created;
        }
        $is_closed = TRUE;
      }
      else {
        $status = $comment->sid;
        $duration = time() - $issue->created;
        $is_closed = FALSE;
      }

      $stages[$last_stage] += $comment->timestamp - $last_date;
      $last_date = $comment->timestamp;
      $last_stage = $comment->sid;
    }

    if (!$is_closed) {
      $stages[$last_stage] += time() - $last_date;
    }

    if ($duration > 0 ) {

      $category_duration[$issue->category]['all'] += $duration;
      $category_count[$issue->category]['all']++;

      $all_duration['all'] += $duration;
      $all_count['all']++;

      if ($issue->created > $year) {
        $category_duration[$issue->category]['year'] += $duration;
        $category_count[$issue->category]['year']++;
        $all_duration['year'] += $duration;
        $all_count['year']++;
      }

      if ($issue->created > $month) {
        $category_duration[$issue->category]['month'] += $duration;
        $category_count[$issue->category]['month']++;
        $all_duration['month'] += $duration;
        $all_count['month']++;
      }

      if ($duration < 2 * 86400) {
        $duration_count['two_days']['all']++;
        if ($issue->created > $year) {
          $duration_count['two_days']['year']++;
        }
        if ($issue->created > $month) {
          $duration_count['two_days']['month']++;
        }
      }
      elseif ($duration < 14 * 86400) {
        $duration_count['two_weeks']['all']++;
        if ($issue->created > $year) {
          $duration_count['two_weeks']['year']++;
        }
        if ($issue->created > $month) {
          $duration_count['two_weeks']['month']++;
        }
      }
      elseif ($duration < 60 * 86400) {
        $duration_count['two_months']['all']++;
        if ($issue->created > $year) {
          $duration_count['two_months']['year']++;
        }
        if ($issue->created > $month) {
          $duration_count['two_months']['month']++;
        }
      }
      elseif ($duration < 180 * 86400) {
        $duration_count['six_months']['all']++;
        if ($issue->created > $year) {
          $duration_count['six_months']['year']++;
        }
        if ($issue->created > $month) {
          $duration_count['six_months']['month']++;
        }
      }
      else {
        $duration_count['over_six_months']['all']++;
        if ($issue->created > $year) {
          $duration_count['over_six_months']['year']++;
        }
        if ($issue->created > $month) {
          $duration_count['over_six_months']['month']++;
        }
      }
      $duration_total['all']++;
      if ($issue->created > $year) {
        $duration_total['year']++;
      }
      if ($issue->created > $month) {
        $duration_total['month']++;
      }

      if ($duration > 60 * 86400 && $issue->created > 730 * 86400) {
        foreach ($stages as $key => $value) {
          $all_stages[$key] += $value;
          $all_stages_count[$key]++;
        }
      }
    }
  }

  $output .= '<h2>'. t('Average lifetime') .'</h2>';

  $header = array(t('Category'), t('Overall'), t('Past Year'), t('Past 30 Days'));
  $rows = array();
  foreach (array_keys($category_count) as $category) {
    $rows[$category] = array(
      project_issue_category($category),
      format_interval($category_duration[$category]['all'] / $category_count[$category]['all']),
      format_interval($category_duration[$category]['year'] / $category_count[$category]['year']),
      format_interval($category_duration[$category]['month'] / $category_count[$category]['month']),
    );
  }
  $rows['all'] = array(
    t('all issues'),
    format_interval($all_duration['all'] / $all_count['all']),
    format_interval($all_duration['year'] / $all_count['year']),
    format_interval($all_duration['month'] / $all_count['month']),
  );
  $output .= theme('table', $header, $rows);

  $output .= '<h2>'. t('Breakdown by lifetime') .'</h2>';

  $header = array(t('Duration'), t('Overall'), t('Past Year'));
  $rows = array();
  foreach (array_keys($duration_count) as $duration) {
    $rows[$duration] = array(
      $duration_count[$duration]['label'],
      t('@num (@pct%)', array(
        '@num' => $duration_count[$duration]['all'],
        '@pct' => round($duration_count[$duration]['all'] / $duration_total['all'] * 100),
      )),
      t('@num (@pct%)', array(
        '@num' => $duration_count[$duration]['year'],
        '@pct' => !empty($duration_total['year']) ? round($duration_count[$duration]['year'] / $duration_total['year'] * 100) : 0,
      )),
    );
  }
  $output .= theme('table', $header, $rows);

  $output .= '<h2>'. t('Breakdown by status') .'</h2>';
  $output .= '<p>' . t('For issues lasting over 60 days opened within the past two years, show average breakdown by issue status.') . '</p>';

  $header = array(t('Status'), t('Duration'));
  $rows = array();
  foreach ($states as $key => $label) {
    if (!empty($all_stages_count[$key])) {
      $rows[] = array(
        $states[$key],
        format_interval($all_stages[$key] / $all_stages_count[$key], 2),
      );
    }
  }
  $output .= theme('table', $header, $rows);


  $output .= '</div>';
  return $output;
}
